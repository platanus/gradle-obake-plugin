gradle obake plugin
======================

[![Build Status](https://drone.io/bitbucket.org/grimrose/gradle-obake-plugin/status.png)](https://drone.io/bitbucket.org/grimrose/gradle-obake-plugin/latest)
[ ![Download](https://api.bintray.com/packages/grimrose/maven/gradle-obake-plugin/images/download.png) ](https://bintray.com/grimrose/maven/gradle-obake-plugin/_latestVersion)

[phantomjs](http://phantomjs.org) Helper plugin for [Gradle](http://www.gradle.org)


## Usage

In your *build.gradle* file add:

```groovy
buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath group: 'org.bitbucket.grimrose', name: 'gradle-obake-plugin', version: '0.5'
    }
}

apply plugin: 'obake'
```

## Prepare PhantomJS

you can use `preparePhantomJS` task before another using **PhantomJS** task.

```bash
$ gradle preparePhantomJS
```

After run `preparePhantomJS` task, task created *$projectDir/tools*.

```bash
$ tree
.
├── build.gradle
├── gradle
│   └── wrapper
├── gradlew
├── gradlew.bat
├── settings.gradle
├── src
│   ├── main
│   │   └── java
│   └── test
│       ├── java
└── tools
    └── phantomjs
        ├── 1.9.7
        │   └── macosx
        │       ├── ChangeLog
        │       ├── LICENSE.BSD
        │       ├── README.md
        │       ├── bin
        │       │   └── phantomjs
                ├── examples
                │   ├── arguments.coffee
                │   ├── arguments.js
...

```

if use current **PhantomJS**, Please configure extension like this.
```groovy
obake {
    phantomjsBinaryPath = 'path/to/phantomjs-binary-directory'
}
```

## Create PhantomJS using Task

`phantomJSAgent` task is bundled. 

please configure input JavaScript file path.

```groovy
phantomJSAgent {
    js = 'tools/phantomjs/1.9.7/macosx/examples/version.js'
}
```

run `phantomJSAgent` task. then you can get result.
```bash
$ gradle phantomJSAgent

using PhantomJS version 1.9.7

BUILD SUCCESSFUL
```

## Sample of QUnit Runner

if define this task, you can run *run-qunit.js*.

In addition, configure specifying the path to **outputFile**, execution results are output to file.

```groovy
task runQunit(type: org.bitbucket.grimrose.gradle.PhantomJSAgentTask) {
    js = 'tools/phantomjs/1.9.7/macosx/examples/run-qunit.js'
    args = [file("$projectDir/src/test/js/index.html")]
    outputFile = file("$buildDir/result.txt")
}
```

## License

Copyright 2013 grimrose.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

  [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


## Contributing
As you would expect, clone, push to repository and create a pull request for us to review and merge.
