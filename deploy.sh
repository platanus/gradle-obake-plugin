#!/bin/bash

set -e

./gradlew clean

./gradlew preparePublication -Penv=deploy

./gradlew publish -Penv=deploy

./gradlew bintrayUpload -Penv=deploy
