package org.bitbucket.grimrose.gradle

import spock.lang.Shared
import spock.lang.Specification


class ObakeConfigRepositorySpec extends Specification {

    @Shared
    PhantomJSVersion version = PhantomJSVersion._1_9_7

    def "expect find url of googlecode"() {
        expect:
        ObakeConfigRepository.create('config.json').findBy(ObakeConfig.KEY_GOOGLE_CODE_URL) == 'https://phantomjs.googlecode.com/files'
        new ObakeConfigRepository().findBy(ObakeConfig.KEY_GOOGLE_CODE_URL) == 'https://phantomjs.googlecode.com/files'
    }

    def "expect find url of bitbucket"() {
        expect:
        ObakeConfigRepository.create('config.json').findBy(ObakeConfig.KEY_BIT_BUCKET_URL) == 'https://bitbucket.org/ariya/phantomjs/downloads'
        new ObakeConfigRepository().findBy(ObakeConfig.KEY_BIT_BUCKET_URL) == 'https://bitbucket.org/ariya/phantomjs/downloads'
    }

    def "version should be find"() {
        given:
        def repository = ObakeConfigRepository.create('config.json')
        def sut = repository.findBy('binary').macosx

        when:
        println(sut)
        def actual1 = sut.any { it.version == version.toString() }
        def actual2 = sut.find { it.version == version.toString() }

        then:
        actual1
        actual2.version == version.toString()
        actual2.checksum
    }

}
