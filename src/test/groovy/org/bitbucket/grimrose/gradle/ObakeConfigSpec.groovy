package org.bitbucket.grimrose.gradle

import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class ObakeConfigSpec extends Specification {

    def "expect current version is 1.9.7"() {
        when:
        def config = new ObakeConfig()

        then:
        config.currentVersion == PhantomJSVersion._1_9_7.toString()
    }

    def "expect specific version is 1.9.2"() {
        given:
        def config = new ObakeConfig()

        when:
        def project = ProjectBuilder.builder().build()
        project.apply plugin: 'obake'

        def extension = project.extensions.findByType(ObakePluginExtension)
        extension.phantomjsVersion = '1.9.2'

        then:
        config.apply(extension).version == PhantomJSVersion._1_9_2
    }

}
