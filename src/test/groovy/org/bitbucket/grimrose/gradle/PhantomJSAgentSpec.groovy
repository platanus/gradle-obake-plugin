package org.bitbucket.grimrose.gradle

import org.apache.commons.lang.SystemUtils
import org.gradle.api.InvalidUserDataException
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification


class PhantomJSAgentSpec extends Specification {

    @Shared
    PhantomJSVersion version = PhantomJSVersion._1_9_7

    @Shared
    Project project


    def setupSpec() {
        project = ProjectBuilder.builder().build()
        project.apply plugin: 'obake'

        def task = project.tasks.findByName('preparePhantomJS') as PreparePhantomJSTask

        ObakeConfig config = Spy(ObakeConfig)
        File archiveResources = new File(SystemUtils.USER_DIR, 'src/test/resources/')
        config.makeDownloadUrl(_) >> new File(archiveResources, config.makeArchiveFileName(version)).toURI().toURL()
        config.apply(project.extensions.findByType(ObakePluginExtension))
        task.config = config

        task.execute()
    }

    PhantomJSAgent sut

    String helloJs
    String argumentsJs

    def setup() {
        ObakeConfig config = new ObakeConfig()

        ConventionPathResolver conventionPathResolver = new ConventionPathResolver()

        def conventionPath = conventionPathResolver.find(project.projectDir, config)
        helloJs = new File(conventionPath, 'examples/hello.js').absolutePath
        argumentsJs = new File(conventionPath, 'examples/arguments.js').absolutePath

        def resolver = new PhantomJSBinaryPathResolver(resolver: conventionPathResolver)
        def path = resolver.find(project.projectDir, config)
        sut = new PhantomJSAgent(binaryPath: path)
    }

    def "agent should execute"() {
        when:
        def actual = sut.execute(helloJs)

        then:
        actual == 'Hello, world!\n'
    }

    def "agent should execute for help"() {
        when:
        def actual = sut.adopt('-h').execute(helloJs)

        println actual

        then:
        actual
    }

    def "agent should execute with option"() {
        when:
        sut.adopt('--remote-debugger-autorun').execute(helloJs)

        then:
        notThrown(InvalidUserDataException)
    }

    def "agent should execute with option and args"() {
        when:
        def actual = sut.adopt('--script-encoding=utf8').execute(argumentsJs, 'a', 'b')

        then:
        actual == """0: $argumentsJs
1: a
2: b
"""
    }

    def "agent should throw exception"() {
        when:
        sut.execute('hoge')

        then:
        def e = thrown(InvalidUserDataException)
        e.message == "Can't open 'hoge'\n"
    }

}
