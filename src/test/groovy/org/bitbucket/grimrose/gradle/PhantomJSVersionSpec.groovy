package org.bitbucket.grimrose.gradle

import spock.lang.Specification

class PhantomJSVersionSpec extends Specification {

    def "support version"() {
        expect:
        PhantomJSVersion._1_9_7.version == "1.9.7"
        PhantomJSVersion.getValue("1.9.7") == PhantomJSVersion._1_9_7
    }

    def "not support version"() {
        when:
        PhantomJSVersion sut = PhantomJSVersion.getValue("")

        then:
        thrown(IllegalArgumentException)
    }

}
