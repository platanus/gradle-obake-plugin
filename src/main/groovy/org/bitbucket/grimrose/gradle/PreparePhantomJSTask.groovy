package org.bitbucket.grimrose.gradle

import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files
import java.nio.file.Paths
import java.security.MessageDigest

class PreparePhantomJSTask extends DefaultTask {

    ObakeConfig config = new ObakeConfig()

    @TaskAction
    def process() {
        def extension = project.extensions.findByType(ObakePluginExtension)
        config.apply(extension)
        def binaryPath = validateBinaryPath()
        logger.lifecycle "phantomjs binary path is [$binaryPath]"
        if (!binaryPath) {
            deploy()
            askVersion()
        }
        logger.lifecycle 'phantomjs is prepared.'
    }

    String validateBinaryPath() {
        def binaryPath = resolveBinaryPath()
        try {
            askVersion(binaryPath)
        } catch (InvalidUserDataException e) {
            logger.warn(e.message)
            return ''
        }
        return binaryPath
    }

    void deploy() {
        def filePath = download()

        unpack(filePath)

        def path = resolveBinaryPath()
        new File(path).setExecutable(true)

        System.setProperty(ObakeConfig.BINARY_PATH_KEY, path)
    }

    void unpack(String archivePath) {
        assert archivePath
        def conventionPath = makeConventionPath()
        archiveExtractor.unpack(project.file(conventionPath), archivePath)
    }

    String download() {
        def version = config.version
        def tmp = project.file(System.properties['java.io.tmpdir'])

        def archiveFile = Paths.get(tmp.toPath().toAbsolutePath().toString(), config.makeArchiveFileName(version))

        Files.deleteIfExists(archiveFile)

        def url = new URL(config.makeDownloadUrl(version))
        logger.debug "download url is $url"

        if (url.protocol == 'file') {
            project.ant.get(
                    src: url,
                    dest: tmp.absolutePath
            )
        } else {
            def request = new HttpGet(url.toURI())
            HttpClient client = new DefaultHttpClient()
            client.execute(request).entity.content.with { stream ->
                Files.copy(stream, archiveFile)
            }
        }

        def file = new File(tmp, config.makeArchiveFileName(version))
        logger.debug "download file is $file"

        def messageDigest = MessageDigest.getInstance('SHA1')
        file.eachByte(1024 * 1024) { byte[] buffer, int bytesRead ->
            messageDigest.update(buffer, 0, bytesRead);
        }
        def sha1Hex = new BigInteger(1, messageDigest.digest()).toString(16).padLeft(40, '0')

        def checksum = config.findCheckSum(version)
        assert checksum == sha1Hex
        logger.debug "checksum is $checksum"

        file.absolutePath
    }

    void askVersion(binaryPath = System.properties[ObakeConfig.BINARY_PATH_KEY]) {
        def bin = new File(binaryPath ?: '')
        if (!bin.canExecute()) throw new InvalidUserDataException("binary path can not execute. [$binaryPath]")

        def process = [bin.absolutePath, '-v'].execute()
        process.waitFor()
        if (process.exitValue()) {
            throw new InvalidUserDataException(process.err.text)
        }
        logger.info(process.in.text)
    }

    String resolveBinaryPath() {
        phantomJSBinaryPathResolver.find(project.projectDir, config)
    }

    String makeConventionPath() {
        conventionPathResolver.find(project.projectDir, config)
    }

    ArchiveExtractor getArchiveExtractor() {
        new ArchiveExtractor(config: config, ant: project.ant)
    }

    ConventionPathResolver getConventionPathResolver() {
        new ConventionPathResolver()
    }

    PhantomJSBinaryPathResolver getPhantomJSBinaryPathResolver() {
        new PhantomJSBinaryPathResolver(resolver: conventionPathResolver)
    }

}
