package org.bitbucket.grimrose.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

class PhantomJSAgentTask extends DefaultTask {

    @Input
    def js

    List options = []

    List args = []

    File outputFile

    @TaskAction
    def process() {

        def opts = (options ?: []).flatten().collect { it.toString() } as String[]
        def jsPath = project.file(js).path
        def arguments = (args ?: []) as String[]

        ObakeConfig config = new ObakeConfig()
        config.apply(project.extensions.findByType(ObakePluginExtension))

        def path = phantomJSBinaryPathResolver.find(project.projectDir, config)

        def result = PhantomJSAgent.make(path).adopt(opts).execute(jsPath, arguments)

        logger.lifecycle result
        if (outputFile) {
            logger.lifecycle("output:[$outputFile]")
            project.file(outputFile).text = result
        }

    }

    PhantomJSBinaryPathResolver getPhantomJSBinaryPathResolver() {
        new PhantomJSBinaryPathResolver(resolver: new ConventionPathResolver())
    }


}
