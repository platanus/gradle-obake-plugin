package org.bitbucket.grimrose.gradle


class ArchiveExtractor {

    ObakeConfig config

    AntBuilder ant

    void unpack(File conventionDir, String archivePath) {
        assert archivePath
        assert conventionDir
        def prefix = config.archivePrefix

        conventionDir.mkdir()

        Closure extract = config.linux ? untar : unzip
        extract archivePath, conventionDir.path, prefix
    }

    Closure untar = { archivePath, conventionPath, prefix ->
        ant.untar(
                src: archivePath,
                dest: conventionPath,
                compression: "bzip2"
        ) {
            patternset {
                include(name: prefix + '/*')
                include(name: prefix + '/bin/*')
                include(name: prefix + '/examples/*')
            }
            mapper {
                globmapper(from: prefix + '/*', to: '*')
                globmapper(from: prefix + '/bin/*', to: '*')
                globmapper(from: prefix + '/examples/*', to: '*')
            }
        }
    }

    Closure unzip = { archivePath, conventionPath, prefix ->
        ant.unzip(
                src: archivePath,
                dest: conventionPath
        ) {
            patternset {
                include(name: prefix + '/*')
                include(name: prefix + '/bin/*')
                include(name: prefix + '/examples/*')
            }
            mapper {
                globmapper(from: prefix + '/*', to: '*')
                globmapper(from: prefix + '/bin/*', to: '*')
                globmapper(from: prefix + '/examples/*', to: '*')
            }
        }
    }

}
