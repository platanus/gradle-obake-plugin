package org.bitbucket.grimrose.gradle

public enum PhantomJSVersion {
    _1_9_1("1.9.1"),
    _1_9_2("1.9.2"),
    _1_9_6("1.9.6"),
    _1_9_7("1.9.7")

    String version

    PhantomJSVersion(String version) {
        this.version = version
    }

    static PhantomJSVersion getValue(String version) {
        def result = this.values().find { it.version == version }
        if (!result) throw new IllegalArgumentException("not support version")
        result
    }

    boolean isDownloadFromGoogleCode() {
        equals(_1_9_1) || equals(_1_9_2)
    }

    @Override
    public String toString() {
        return version
    }

}
