package org.bitbucket.grimrose.gradle

import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

class PhantomJSBinaryPathResolver {

    Logger logger = Logging.getLogger(PhantomJSBinaryPathResolver)

    ConventionPathResolver resolver

    String find(File projectDir, ObakeConfig config) {
        if (config.specificBinaryPath) return config.specificBinaryPath

        def conventionPath = resolver.find(projectDir, config)

        def path = config.makeBinaryPath(conventionPath)
        logger.debug "binary path is $path"
        path
    }

}
