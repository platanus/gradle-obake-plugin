package org.bitbucket.grimrose.gradle

import org.apache.commons.lang.SystemUtils

class ObakeConfig {

    static final String BINARY_PATH_KEY = 'phantomjs.binary.path'

    static final String WINDOWS = 'windows'
    static final String MACOSX = 'macosx'
    static final String LINUX_32 = 'linux-i686'
    static final String LINUX_64 = 'linux-x86_64'

    static final String KEY_GOOGLE_CODE_URL = 'googlecode-url'
    static final String KEY_BIT_BUCKET_URL = 'bitbucket-url'

    ObakeConfigRepository repository = new ObakeConfigRepository()

    ObakePluginExtension extension

    String getToolsPath() {
        repository.findBy("phantom-js-binary-path-convention")?.toString()
    }

    String getCurrentVersion() {
        repository.findBy("current-version")?.toString()
    }

    String getPlatform() {
        if (SystemUtils.IS_OS_WINDOWS) return WINDOWS
        if (SystemUtils.IS_OS_MAC_OSX) return MACOSX
        if (SystemUtils.IS_OS_LINUX) {
            if ('uname -m'.execute().text.endsWith('64')) return LINUX_64
            return LINUX_32
        }
        throw new UnsupportedOperationException('phantomjs binary not supported.')
    }

    boolean isWindows() {
        WINDOWS == platform
    }

    boolean isLinux() {
        [LINUX_32, LINUX_64].contains(platform)
    }

    String makeDownloadUrl(version) {
        String key = PhantomJSVersion.getValue("$version").downloadFromGoogleCode ? KEY_GOOGLE_CODE_URL : KEY_BIT_BUCKET_URL
        def baseUrl = repository.findBy(key)
        return "$baseUrl/${makeArchiveFileName(version)}"
    }

    String makeArchiveFileName(version) {
        String extension = linux ? '.tar.bz2' : '.zip'
        return "${makeArchivePrefix(version)}${extension}"
    }

    String makeArchivePrefix(version) {
        assert isValidVersion(version)
        return "phantomjs-$version-${platform}"
    }

    String getArchivePrefix() {
        assert isValidVersion(version)
        return "phantomjs-$version-${platform}"
    }

    String makeBinaryPath(String parentPath) {
        def path = makePrefixPath(parentPath)
        makeSuffixPath(path)
    }

    String makePrefixPath(String dirPath) {
        new File(dirPath).absolutePath + (windows ? '' : '/bin')
    }

    String makeSuffixPath(String dirPath) {
        new File(dirPath, 'phantomjs' + (windows ? '.exe' : '')).absolutePath
    }

    boolean isValidVersion(version) {
        findBinaryInformation(version) != null
    }

    String findCheckSum(version) {
        findBinaryInformation(version)?.checksum
    }

    private Map findBinaryInformation(version) {
        repository.findBy('binary')[platform].find { it.version == "$version" }
    }

    ObakeConfig apply(ObakePluginExtension extension) {
        this.extension = extension
        this
    }

    PhantomJSVersion getVersion() {
        PhantomJSVersion.getValue(extension?.phantomjsVersion ?: currentVersion)
    }

    String getSpecificBinaryPath() {
        extension?.phantomjsBinaryPath
    }

}
