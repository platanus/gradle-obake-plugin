package org.bitbucket.grimrose.gradle

import groovy.text.SimpleTemplateEngine
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging


class ConventionPathResolver {

    Logger logger = Logging.getLogger(ConventionPathResolver)

    String find(File projectDir, ObakeConfig config) {
        def version = config.version
        def platform = config.platform

        def binding = [projectDir: projectDir, version: version, platform: platform]
        def conventionPath = new SimpleTemplateEngine().createTemplate(config.toolsPath).make(binding).toString()
        logger.debug("convention path is $conventionPath")
        conventionPath
    }

}
