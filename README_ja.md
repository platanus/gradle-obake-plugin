gradle obake plugin
======================

[![Build Status](https://drone.io/bitbucket.org/grimrose/gradle-obake-plugin/status.png)](https://drone.io/bitbucket.org/grimrose/gradle-obake-plugin/latest)
[ ![Download](https://api.bintray.com/packages/grimrose/maven/gradle-obake-plugin/images/download.png) ](https://bintray.com/grimrose/maven/gradle-obake-plugin/_latestVersion)

[phantomjs](http://phantomjs.org)のバイナリファイルを[Gradle](http://www.gradle.org)で利用できるようにするためのpluginです。

## 使い方

このプラグインを使うには、*build.gradle* に以下のレポジトリと依存関係を追加してください。

```groovy
buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath group: 'org.bitbucket.grimrose', name: 'gradle-obake-plugin', version: '0.5'
    }
}

apply plugin: 'obake'
```


## phantomjsを準備する

*buildscript* を設定すると **preparePhantomJS** タスクを利用できるようになります。

```bash
$ gradle preparePhantomJS 

```

**preparePhantomJS** タスクを実行すると*$projectDir* 配下に以下のようなファイルが展開されます。

```bash
$ tree
.
├── build.gradle
├── gradle
│   └── wrapper
├── gradlew
├── gradlew.bat
├── settings.gradle
├── src
│   ├── main
│   │   └── java
│   └── test
│       ├── java
└── tools
    └── phantomjs
        ├── 1.9.7
        │   └── macosx
        │       ├── ChangeLog
        │       ├── LICENSE.BSD
        │       ├── README.md
        │       ├── bin
        │       │   └── phantomjs
                ├── examples
                │   ├── arguments.coffee
                │   ├── arguments.js
...

```

また、既にphantomjsを別Directoryにて設定している場合は、*build.gradle* 以下のように設定することで利用できるようになります。

```groovy
obake {
	phantomjsBinaryPath = 'path/to/phantomjs-binary-directory'
}
```


## phantomjsを実行する

例えば、まず以下のように実行するjsファイルのパスを指定します。

```groovy
phantomJSAgent {
    js = 'tools/phantomjs/1.9.7/macosx/examples/version.js'
}
```

次に、以下のコマンドを実行します。
```bash
$ gradle phantomJSAgent
```

結果として、以下のように表示されれば、phantomjsは実行されています。

```bash
using PhantomJS version 1.9.7

BUILD SUCCESSFUL
```


## QUnitのrunnerを実行する
以下のように、タスクを定義することで、*run-qunit.js* を実行することが出来ます。
また、**outputFile** にパスを指定すると実行結果がファイル出力されます。

```groovy
task runQunit(type: org.bitbucket.grimrose.gradle.PhantomJSAgentTask) {
    js = 'tools/phantomjs/1.9.7/macosx/examples/run-qunit.js'
    args = [file("$projectDir/src/test/js/index.html")]
    outputFile = file("$buildDir/result.txt")
}
```


## ライセンス

Copyright 2013 grimrose.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

  [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


## 最後に
issue reportとpull requestをお待ちしております。
